const { monthInLetters } = require('../main.js')

test ("Comprobamos que devuelve bien los meses", function() {
    expect(monthInLetters(1)).toBe("enero");
    expect(monthInLetters(11)).toBe("noviembre");
    expect(monthInLetters(13)).toBe("error en la recepción del mes");
});