const state = {
    searchButton: document.getElementById("searchButton"),
    body: document.getElementById("body"),
    inicio: document.getElementById("inicio"),
    display1: document.getElementById("display1"),
    display2: document.getElementById("display2"),
    city: null,
    ticketmasterBaseUrl: "https://app.ticketmaster.com/discovery/v2/events.json?apikey=",
    ticketmasterApiKey: "ITBSTLmYsxAwpM7RdgS87rIlkdh5b1LN",
    urlTicketmaster: null,
    ticketmasterInitialData: null,
    ticketmasterData: null,
    ticketmasterEvents: null,    
    eventObject: null,

    weatherstackBaseUrl: "http://api.weatherstack.com/current?access_key=",
    weatherstackApiKey: "b2c603f4c6d15ab2f4278cf3e441a063",
    weatherstackURL: null,
    weatherstackQuery: null,
    weatherstackData: null,
    localTime: null,
    longitude: null,
    latitude: null,
    date: Date.now(),
    newsApiBaseUrl: "https://newsapi.org/v2/everything?q=",
    newsApiKey: "005858644ec7472c85f2830d98d291fc",
    urlNewsAPI: null,
    urlApiNews: null,
    apiNewsData: null,
    listSize: 5,
    createdText: null,
    mapboxAccessToken: 'pk.eyJ1IjoiZWxiMDAyeCIsImEiOiJjazU2bXNwN3kwNWUwM3FwZWkycXZsOHU2In0.83-yLRR7V63w3Mfzq3iQ7g'
}

function KeyEnter() {
    var searchInput = document.getElementById("search");
    searchInput.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            document.getElementById("searchButton").click();
        }
    })
}

state.searchButton.addEventListener('click', function() {
    search(state)
});
state.body.addEventListener('load',showDisplay1);
state.body.addEventListener('load',KeyEnter);
state.inicio.addEventListener('click',showDisplay1);

async function search(state) {
    state.city = document.getElementById("search").value;
	if(state.city === null || state.city.length === 0){
		alert("No has introducido nada")
		return false;
	} else {
        generateWeatherstackURL(state);
        await getWeatherstackData(state);
        await generateNewsURL(state);
        await getNewsData(state);
        showDisplay2(state);
        showCityInfo(state);
        generateTicketmasterURL(state);
        await getTicketmasterData(state);
        await assignTicketmasterEvents(state);
        await printTicketmaterEvents(state);
	}
}

function generateNewsURL(state) {
    state.urlNewsAPI = state.newsApiBaseUrl + state.city + "&from=" + state.date + "&sortBy=publishedAt&apiKey=" + state.newsApiKey;
}

async function getNewsData(state) {
    document.getElementById("newsList").innerHTML = "";
    state.urlApiNews = await fetch(state.urlNewsAPI);
    state.apiNewsData = await state.urlApiNews.json();
    document.getElementById("newsList").insertAdjacentHTML('beforeend', "<lh class='listTitle'>Noticias</lh>");

    for(i=0;i<state.listSize;i++) {
        state.createdText = "<a href='" + state.apiNewsData.articles[i].url + "'>" + state.apiNewsData.articles[i].title + "</a>";
        document.getElementById("newsList").insertAdjacentHTML('beforeend', "<li>" + state.createdText + "</li>");
    }
}

function generateTicketmasterURL(state) {
    state.urlTicketmaster = state.ticketmasterBaseUrl + state.ticketmasterApiKey + "&city=" + state.city;
}

async function getTicketmasterData(state){
    document.getElementById("eventsList").innerHTML = "";
    state.ticketmasterInitialData = await fetch(state.urlTicketmaster);
    state.ticketmasterEvents = await state.ticketmasterInitialData.json();
    if(state.ticketmasterEvents.page.totalElements === 0) {
        document.getElementById("eventsList").insertAdjacentHTML('beforeend', "<li>No hay eventos disponibles en esta ciudad.</li>");
    }   
}

async function assignTicketmasterEvents(state) {
    //var localTicketmasterData = await state.ticketmasterData;
    state.eventObject = 
        await state.ticketmasterEvents._embedded.events.map(
            event => (
                {
                    name: event.name,
                    venue: event._embedded.venues[0].name,
                    date: changeDateFormat(event.dates.start.localDate)
                }
            )
        )
}

async function printTicketmaterEvents(state) {
    //var localTicketmasterEvents = await state.ticketmasterEvents;
    console.log("Cantidad de eventos: " + state.eventObject.length)
    document.getElementById("eventsList").insertAdjacentHTML('beforeend', "<lh class='listTitle'>Eventos</lh>");
    for(var i=0;i<state.listSize;i++) {
        document.getElementById("eventsList").insertAdjacentHTML('beforeend', "<li>" + state.eventObject[i].name + " en " + state.eventObject[i].venue + " el " + state.eventObject[i].date + ".</li>");
    }
}

function generateWeatherstackURL(state) {
    state.urlWeatherstack = state.weatherstackBaseUrl + state.weatherstackApiKey + "&query=" + state.city;    
}

async function getWeatherstackData(state) {
    state.weatherstackQuery = await fetch(state.urlWeatherstack);
    state.weatherstackData = await state.weatherstackQuery.json();
    console.log(state.weatherstackData)
    
    document.getElementById("cityCountry").innerHTML = state.weatherstackData.location.name + ", " + state.weatherstackData.location.country;
    document.getElementById("weatherIcons").innerHTML="<img src='" + state.weatherstackData.current.weather_icons + "' width='80px'>";
    document.getElementById("weatherDescriptions").innerHTML = state.weatherstackData.current.weather_descriptions;
    state.localTime = state.weatherstackData.location.localtime;
	state.latitude = state.weatherstackData.location.lat[0] + "" + state.weatherstackData.location.lat[1] + "" + state.weatherstackData.location.lat[2] + "" + state.weatherstackData.location.lat[3] + "" + state.weatherstackData.location.lat[4] + "" + state.weatherstackData.location.lat[5];
	state.longitude = state.weatherstackData.location.lon[0] + "" + state.weatherstackData.location.lon[1] + "" + state.weatherstackData.location.lon[2] + "" + state.weatherstackData.location.lon[3] + "" + state.weatherstackData.location.lon[4] + "" + state.weatherstackData.location.lon[5];
    document.getElementById("time").innerHTML = state.localTime[11] + "" + state.localTime[12] + "" + state.localTime[13] + "" + state.localTime[14] + "" + state.localTime[15];
    document.getElementById("date").innerHTML=changeDateFormat(state.localTime);
    document.getElementById("temperature").innerHTML = state.weatherstackData.current.temperature+"°C";
    map(state);
}

function showDisplay1() {
    if (state.display1.classList.contains('hideDisplay')) {
        state.display1.classList.remove = "hideDisplay";
        state.display1.classList.add = "showDisplay";
        state.display2.classList.remove = "showDisplay"
        state.display2.classList.add = "hideDisplay";
    }
}

function showDisplay2() {
    if (state.display1.classList.contains('showDisplay')) {
        state.display1.classList.remove = "showDisplay";
        state.display1.classList.add = "hideDisplay";
        state.display2.classList.remove = "hideDisplay"
        state.display2.classList.add = "showDisplay";
    }
}

function changeDateFormat(englishDate) {
    var splitDate = englishDate.split("-");
    
    for(var i=0;i<splitDate.length;i++) {
        splitDate[i] = parseInt(splitDate[i], 10);
    }

    return splitDate[2] + " de " + monthInLetters(splitDate[1]) + " de " + splitDate[0];
}

function monthInLetters(month) {
    switch(month) {
        case 1:
            return "enero";
        case 2: 
            return "febrero";
        case 3:
            return "marzo";
        case 4: 
            return "abril";
        case 5:
            return "mayo";
        case 6:
            return "junio";
        case 7:
            return "julio";
        case 8:
            return "agosto";
        case 9:
            return "septiembre";
        case 10:
            return "octubre";
        case 11:
            return "noviembre";
        case 12: 
            return "diciembre";
        default:
            return "error en la recepción del mes";
    }
}

/*try {
    module.exports = { devuelveUno };
} catch {
    console.log("");
}*/

function showCityInfo(){
    document.getElementById("display2").style.display="block";
    document.getElementById("display1").style.display="none";
}

function map(state){
    mapboxgl.accessToken = state.mapboxAccessToken;
	var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [state.longitude, state.latitude],
        zoom: 10
    });
}